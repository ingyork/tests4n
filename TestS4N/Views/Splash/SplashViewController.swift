//
//  SplashViewController.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var logo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        removeStackNavigationWithoutSelf()
        navigationController?.navigationBar.isHidden = true
    }
    
    func removeStackNavigationWithoutSelf() {
        var index = 0
        navigationController?.viewControllers.forEach({ (controller) in
            if !(controller is SplashViewController) {
                navigationController?.viewControllers.remove(at: index)
            }
            index+=1
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initAnimation()
    }
    
    
    func initAnimation(){
        UIView.animate(withDuration: 0.1, delay: 1, options: [], animations: {
            self.logo.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
        }) { (Bool) in
            self.secondAnimation()
        }
    }
    
    func secondAnimation(){
        UIView.animate(withDuration: 0.1, animations: {
            self.logo.transform = .identity
        }) { (finished) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.navigateToHome()
            }
        }
    }
    func navigateToHome() {
        let controller = HomeViewController()
        navigationController?.pushViewController(controller, animated: true)
        navigationController?.viewControllers.remove(at: 0)
    }

}
