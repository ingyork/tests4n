//
//  HomeViewController.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var segmentView: UISegmentedControl!
    @IBOutlet weak var mainContent: UIView!
    
    var currentViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChildViewController(segmentView.selectedSegmentIndex)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func actionSegment(_ sender: Any) {
        addChildViewController(segmentView.selectedSegmentIndex)
    }
    
    @IBAction func actionSegmentView(_ sender: Any) {
        addChildViewController(segmentView.selectedSegmentIndex)
    }
    
    private func addChildViewController(_ segmentIndex: Int) {
        switch segmentView.selectedSegmentIndex {
        case 0:
            removeViewController()
            currentViewController = InputViewController()
            guard let controller = currentViewController else { return }
            addChild(controller)
            mainContent.addSubview(controller.view)
            controller.didMove(toParent: self)
            
        case 1:
            removeViewController()
            currentViewController = HistoryCollectionViewController(collectionViewLayout: UICollectionViewFlowLayout())
            guard let controller = currentViewController else { return }
            addChild(controller)
            mainContent.addSubview(controller.view)
            controller.didMove(toParent: self)
            break
            
        default:
            break
        }
    }
    
    private func removeViewController() {
        guard let controller = currentViewController else { return }
        controller.willMove(toParent: nil)
        // Remove the child
        controller.removeFromParent()
        // Remove the child view controller's view from its parent
        controller.view.removeFromSuperview()
    }

}
