//
//  HistoryCollectionViewController.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit
import RxSwift

private let reuseIdentifier = "Cell"

class HistoryCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var subscriptions = [Disposable]()
    var data = [UserEntity]()
    var progress: UIView!
    
    private var viewModel = UserViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Listado de Registros Históricos"
        collectionView.backgroundColor = .white
        
        // Register cell classes
        self.collectionView!.register(UINib(nibName: "HistoryViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        _ = appDelegateObservable.subscribe(onNext: { (state) in
            switch state {
            case .willEnterBackground:
                self.unsubscribe()
            case .willEnterForeground:
                self.initSubscribe()
            default: break
            }
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initSubscribe()
        viewModel.dispatchLoadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribe()
    }


    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return data.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HistoryViewCell
    
        let d = data[indexPath.row]
        cell.txtName.text = d.name
        cell.txtAge.text = "\(d.age)"
        cell.txtGender.text = d.gender
        cell.txtIsHeadache.text = d.isHeadache ? "SÍ" : "NO"
        cell.txtIsDrinkAlcohol.text = d.isDrinkAlcohol ? "SÍ" : "NO"
        cell.percentage.text = "\(d.percentage)"
        
        if d.percentage >= 0 && d.percentage < 50 {
            cell.imgStatus.image = #imageLiteral(resourceName: "happy")
        }
        else if d.percentage >= 50 && d.percentage < 100 {
            cell.imgStatus.image = #imageLiteral(resourceName: "worried")
        }
        else {
            cell.imgStatus.image = #imageLiteral(resourceName: "sad")
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

}


extension HistoryCollectionViewController {
    func initSubscribe(){
        subscriptions.append(
            UserViewModel.subjectData.subscribe(onNext: { [weak self] (response) in
                self?.data = response
                self?.collectionView.reloadData()
            })
        )
    }
    
    func unsubscribe(){
        subscriptions.forEach({ $0.dispose() })
    }
    
    /*func selectCell(index: Int) {
        let controller = ProspectDetailViewController()
        controller.prospectId = data[index].id
        navigationController?.pushViewController(controller, animated: true)
    }*/
}
