//
//  HistoryViewCell.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit

class HistoryViewCell: UICollectionViewCell {

    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtAge: UILabel!
    @IBOutlet weak var txtGender: UILabel!
    @IBOutlet weak var txtIsHeadache: UILabel!
    @IBOutlet weak var txtIsDrinkAlcohol: UILabel!
    @IBOutlet weak var percentage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
