//
//  InputViewController.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit
import RxSwift

class InputViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var agePicker: UIPickerView!
    @IBOutlet weak var swHeadache: UISwitch!
    @IBOutlet weak var swAlcohol: UISwitch!
    @IBOutlet weak var swGender: UISwitch!
    
    
    private var subscriptions = [Disposable]()
    private var viewModel = UserViewModel()
    
    private var age: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateAgeArray()
        setupView()
        
        
        _ = appDelegateObservable.subscribe(onNext: { (state) in
            switch state {
            case .willEnterBackground:
                self.unsubscribe()
            case .willEnterForeground:
                self.initSubscribe()
            default: break
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initSubscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribe()
    }
    
    private func setupView() {
        agePicker.delegate = self
        agePicker.dataSource = self
    }
    
    private func populateAgeArray() {
        for i in 1...100 {
            age.append(i)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return age.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(age[row])"
    }
    
    private func calculate() {
        let info = getInfoFromForm()
        validateName(name: info.name ?? "", callback: {
            self.viewModel.dispatchCalculation(info: info)
        }) {
            self.showAlertValidationName()
        }
        
    }
    
    
    private func getInfoFromForm() -> UserEntity {
        let fullName = txtName.text
        let age = self.age[agePicker.selectedRow(inComponent: 0)]
        
        let isHeadache = swHeadache.isOn
        let isAlcohol = swAlcohol.isOn
        let gender = swGender.isOn
        
        let info = UserEntity()
        info.name = fullName
        info.gender = gender ? "Femenino" : "Masculino"
        info.age = age
        info.isHeadache = isHeadache
        info.isDrinkAlcohol = isAlcohol
        setPercentage(info: info)
        return info
    }
    
    private func validateName(name: String, callback: @escaping ()-> Void, callbackNotSuccess: @escaping ()-> Void) {
        if name.trimmingCharacters(in: .whitespaces) == "" {
            callbackNotSuccess()
        }
        else {
            callback()
        }
    }
    
    private func showAlertValidationName() {
        let alert = UIAlertController(title: "Error de Validación", message: "Debe ingresar el nombre", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func actionCalculate(_ sender: Any) {
        calculate()
    }
    
    
    private func setPercentage(info: UserEntity){
        var count = 0
        var response: Double = 0
        if info.age >= 20 && info.age <= 30 {
            count += 1
        }
        if info.isDrinkAlcohol {
            count += 1
        }
        if info.isHeadache {
            count += 1
        }
        if info.gender == "Femenino" {
            count += 1
        }
        
        
        if count > 0 {
            response = Double(100) / (Double(4) / Double(count))
        }
        else {
            response = 0
        }
        
        info.percentage = response
    }
}


extension InputViewController {
    func initSubscribe(){
        subscriptions.append(
            
        UserViewModel.subject.subscribe(onNext: { [weak self](status) in
                if status {
                    let controller = ResponseViewController()
                    controller.info = self?.getInfoFromForm()
                    self?.navigationController?.pushViewController(controller, animated: true)
                }
            })
        )
    }
    
    func unsubscribe(){
        subscriptions.forEach({ $0.dispose() })
    }
}
