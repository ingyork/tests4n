//
//  ResponseViewController.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import UIKit

class ResponseViewController: UIViewController {

    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var txtStatus: UILabel!
    
    var info: UserEntity? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        validateStatus()
    }
    
    private func showInfoStatusHappy(percentage: Double) {
        imgStatus.image = #imageLiteral(resourceName: "happy")
        txtStatus.text = "Probabilidad de \(percentage)%, usted puede estar tranquilo de no tener el Síndrome de Kann"
    }
    
    private func showInfoStatusWorried(percentage: Double) {
        imgStatus.image = #imageLiteral(resourceName: "worried")
        txtStatus.text = "Probabilidad de \(percentage)% de tener el Síndrome de Kann"
    }
    
    private func showInfoStatusSad(percentage: Double) {
        imgStatus.image = #imageLiteral(resourceName: "sad")
        txtStatus.text = "Usted tiene \(percentage)% de probabilidades de tener el Síndrome de Kann"
    }
    
    private func validateStatus() {
        if let info = info {
            
            let percentage = info.percentage
            
            if percentage >= 0 && percentage < 50 {
                showInfoStatusHappy(percentage: percentage)
            }
            else if percentage >= 50 && percentage < 100 {
                showInfoStatusWorried(percentage: percentage)
            }
            else {
                showInfoStatusSad(percentage: percentage)
            }
        }
    }
}
