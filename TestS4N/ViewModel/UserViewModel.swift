//
//  UserViewModel.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Foundation
import RxSwift

class UserViewModel {
    
    static let subject = PublishSubject<Bool>()
    static let subjectData = PublishSubject<[UserEntity]>()
    private let repository = DataUserRepository()
    
    func dispatchCalculation(info: UserEntity) {
        repository.saveInfo(info: info)
    }
    
    func dispatchLoadData() {
        repository.getAllData()
    }
}
