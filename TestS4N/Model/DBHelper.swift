//
//  RealmHelper.swift
//  TurismoAppIOS
//
//  Created by Jorge Castro on 16/12/18.
//  Copyright © 2018 Soluciones One Peach. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import RxSwift

class RealmHelper {
    
    var realm: Realm!
    
    init() {
        realm = try! Realm()
    }
    
    func insertRow<T>(_ item: T){
        try! self.realm.write {
            self.realm.create(T.self as! Object.Type, value: item, update: true)
        }
    }
    
    func insertRows<T>(_ array: [T]){
        try! self.realm.write {
            array.forEach({
                self.realm.create(T.self as! Object.Type, value: $0, update: true)
            })
        }
    }
    
    func inserRowsObservable<T>(_ item: T)-> Observable<Bool> {
        var response = true
        let result = try? self.realm.write {
            self.realm.create(T.self as! Object.Type, value: item, update: true)
        }
        if let _ = result {
            response = true
        }
        else {
            response = false
        }
        
        return Observable.create { observer in
            observer.onNext(response)
            return Disposables.create()
        }
    }
    
    func getRows<T: Object>() -> Results<T>? {
        return realm.objects(T.self)
    }
    
    func getRowBy<T: Object>(id: Int64) -> T? {
        return realm.objects(T.self).filter("id = \(id)").first
    }
    
    func getRowsByFilter<T: Object>(filter: String) -> Results<T>? {
        return realm.objects(T.self).filter(filter)
    }
    
    func getRowsByFilterArray<T: Object>(column: String, _ filterArray: [ String]) -> Results<T>? {
        var predicates = [NSPredicate]()
        filterArray.forEach { (filter) in
            predicates.append(NSPredicate(format: "%@ = %@", column, filter))
        }
        let query = NSCompoundPredicate(type: .or, subpredicates: predicates)
        return realm.objects(T.self).filter(query)
    }
    
    
    func getRowsByFilterArray<T: Object>(column: String, _ filterArray: [ Int]) -> Results<T>? {
        var predicates = [NSPredicate]()
        filterArray.forEach { (filter) in
            predicates.append(NSPredicate(format: "%@ = %@", column, NSNumber(value: filter)))
        }
        let query = NSCompoundPredicate(type: .or, subpredicates: predicates)
        return realm.objects(T.self).filter(query)
    }
    
    func delete(_ obj: Object) {
        try! realm.write {
            realm.delete(obj)
        }
    }
    
    func deleteAll(){
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func isEmptyTable<T>(obj: T) -> Bool {
        return realm.objects(obj.self as! Object.Type).isEmpty
    }
    
    func isNotEmptyTable<T>(obj: T) -> Bool {
        return !realm.objects(obj.self as! Object.Type).isEmpty
    }
    
}
