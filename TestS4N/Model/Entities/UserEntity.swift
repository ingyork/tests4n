//
//  UserEntity.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Foundation
import RealmSwift

class UserEntity: Object {
    
    @objc dynamic var id: Int64 = 0
    @objc dynamic var name: String? = nil
    @objc dynamic var age: Int = 18
    @objc dynamic var gender: String? = nil
    @objc dynamic var isHeadache: Bool = false
    @objc dynamic var isDrinkAlcohol: Bool = false
    @objc dynamic var percentage: Double =  0.0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
