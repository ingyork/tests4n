//
//  DataUserRepository.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

class DataUserRepository: DataUserProtocolRepository {

    func saveInfo(info: UserEntity){
        let db = RealmHelper()
        
        if let entity: Results<UserEntity> = db.getRows() {
            info.id = Int64(entity.count + 1)
        }
        
        db.inserRowsObservable(info).subscribe(onNext: { (status) in
            UserViewModel.subject.onNext(status)
        })
    }
    
    func getAllData() {
        let db = RealmHelper()
        var entities = [UserEntity]()
        if let response: Results<UserEntity> = db.getRows() {
            for item in response {
                entities.append(item)
            }
        }
        UserViewModel.subjectData.onNext(entities)
    }
}
