//
//  IDataUserRepository.swift
//  TestS4N
//
//  Created by Jorge Castro on 12/16/18.
//  Copyright © 2018 JorgeCastro. All rights reserved.
//

import Foundation
import RxSwift

protocol DataUserProtocolRepository {
    func saveInfo(info: UserEntity)
    func getAllData()
}
